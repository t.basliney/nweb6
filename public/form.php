<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<title>Задание 6</title>
		<link rel="stylesheet" type="text/css" href="style.css"">
	</head>
	<body>
		<div class="Form">

          <p><a href="admin.php">Администратор</a></p>
        
        <div id="events">
				<p>Добро пожаловать!Заполните пожалуйста следующую форму с данными:</p>
			</div>
			<div id="forma">
			<form method="post" action="index.php" name="contract" >
                <?php
                unset($_SERVER['PHP_AUTH_USER']);
                unset($_SERVER['PHP_AUTH_PW']);
                if(empty($_SESSION['login'])){
                    print('Авторизуйтесь по логину и паролю <a href="login.php">здесь</a> .</br>');
                }
                if(!empty($messages['login_and_password'])){
                    print($messages['login_and_password']);
                }
                if (!empty($messages['save'])) {
                    print($messages['save']);
                }
                ?>
				<div id="nam">
                    <?php
                    $ERROR='';
                    $name='';
                    if (!empty($messages['name'])) {
                        print($messages['name']);
                        $ERROR='error';
                    }
                    if(!empty($values['name'])){
                        $name=$values['name'];
                    }
                    ?>
					Name:<input maxlength="25" size="40" name="name" placeholder="First name" class="<?php print $ERROR?>" value="<?php print $name?>">
				</div>
                </br>
			<div id="address">
                    <?php
                    $ERROR='';
                    $mail='';
                    if (!empty($messages['mail'])) {
                        print($messages['mail']);
                        $ERROR='error';
                    }
                    if(!empty($values['mail'])){
                        $mail=$values['mail'];
                    }
                    ?>
					E-mail:<input name="mail" value="<?php print $mail?>" class="<?php print $ERROR?>" placeholder="mail@gmail.com">
				</div>
                </br>
				<div id="BIRTHYEAR">
                    <?php
                    $ERROR='';
                    if (!empty($messages['year'])) {
                        print($messages['year']);
                        $ERROR='error';
                    } ?>
                    Year of Birth:
                    <span class="<?php print $ERROR?>">
                        <select name="year" size="1">
                            <?php
                            $select=array(1900-2000=>'',2000-2005=>'',2006-2011=>'',2012-2017=>'',2018-2020=>'');
                            for($s=1900-2000;$s<=2018-2020;$s++){
                                if($values['year']==$s){
                                    $select[$s]='selected';break;
                                }
                            }
                            ?>
                            <option value="">...</option>
							<option value="2000" <?php print $select[1900-2000]?>>2000</option>
                            <option value="2001" <?php print $select[2000-2005]?>>2001</option>
                            <option value="2002" <?php print $select[2006-2011]?>>2002</option>
                            <option value="2013" <?php print $select[2012-2017]?>>2003</option>
                            <option value="2014" <?php print $select[2018-2020]?>>2004</option>
                        </select>
                    </span>
				</div>
                </br>
				<div id="SEX">
                    <?php
                    $ERROR='';
                    $sex='';
                    if (!empty($messages['sex'])) {
                        print($messages['sex']);
                        $ERROR='error';
                    }
                    if(!empty($values['sex'])){
                        $sex=$values['sex'];
                    }
                    ?>
                Sex:    <span class="<?php print $ERROR?>">
                            <input type="radio" value="M" name="sex"<?php if($sex=='M') {print'checked';} ?> >Man
                            <input type="radio" value="F" name="sex"<?php if($sex=='F') {print'checked';} ?> >Female
                    </span>
                </div>
                </br>
                <div id="LIMBS">
                    <?php
                    $ERROR='';
                    if (!empty($messages['limbs'])) {
                        print($messages['limbs']);
                        $ERROR='error';
                    }
                    ?>
                    Limbs:<?php
                    $select_limbs=array(1=>'',2=>'',2=>'',3=>'',4=>'');
                    if(!empty($values['limbs'])){
                        for($s=1;$s<=4;$s++){
                            if($values['limbs']==$s){
                                $select_limbs[$s]='checked';break;
                            }
                        }
                    }
                    ?>
                    <span class="<?php print $ERROR?>">
                        <input type="radio" value="1" name="limbs" <?php print $select_limbs[1]?>>1
                        <input type="radio" value="2" name="limbs" <?php print $select_limbs[2]?>>2
                        <input type="radio" value="3" name="limbs" <?php print $select_limbs[3]?>>3
                        <input type="radio" value="4" name="limbs" <?php print $select_limbs[4]?>>4
                    </span>
                </div>
                </br>

                <div id="SUPERPOWERS" >
                    <?php
                    $ERROR='';
                    if(!empty($messages['super'])){
                        print($messages['super']);
                        $ERROR='error';
                    }?>
                    <span >
                        Superpowers:</br>
                       <?php
                         if(!empty($values['super'])){
                             $flag=FALSE;
                             $super_PROVERKA = array("net" =>"", "godmod" =>"", "levitation" =>"", "unvisibility" =>"", "telekinesis" =>"", "extrasensory" =>"");
                             $super = unserialize($values['super']);
                            if(!empty($super))foreach ($super as $E){
                                if($E=="net"){
                                    $super_PROVERKA["net"]="selected";
                                $flag=TRUE;break;}
                            }
                            if(!empty($super))
                                    if(!$flag){
                                        foreach ($super as $T){
                                            $super_PROVERKA["$T"]="selected";
                                        }
                                    }
                         }
                        ?>
                        <select id="sposobnost" name="super[]" multiple="multiple" size="3" class="<?php print $ERROR?>">
                            <option value="net" <?php if(!empty($values['super'])) print $super_PROVERKA["net"]?>>None</option>
                            <option value="godmod"<?php if(!empty($values['super'])) print $super_PROVERKA["godmod"]?> >GodMode</option>
                            <option value="levitation"<?php if(!empty($values['super'])) print $super_PROVERKA["levitation"]?> >Levitation</option>
                            <option value="unvisibility"<?php if(!empty($values['super'])) print $super_PROVERKA["unvisibility"]?> >Invisibility</option>
                            <option value="telekinesis"<?php if(!empty($values['super'])) print $super_PROVERKA["telekinesis"]?> >Telekinesis</option>
                            <option value="extrasensory"<?php if(!empty($values['super'])) print $super_PROVERKA["extrasensory"]?> >Extrasensory</option>
                        </select>
                    </span>
                </div>
                </br>
                    <div id="biography">
                        <?php
                        $ERROR='';
                        $BIO='';
                        if (!empty($messages['biography'])) {
                            print($messages['biography']);
                            $ERROR='error';
                        }
                        if(!empty($values['biography'])){
                            $BIO=$values['biography'];
                        }
                        ?>
                        <p class="<?php print $ERROR?>" >
                            <textarea cols="45" name="biography" placeholder="Here is your brief biography..."><?php if($BIO!='')print $BIO;?></textarea>
                        </p>
                    </div>
                </br>
                    <div id="check1"  >
                    <?php
                    $ERROR='';
                    $check1='';
                    if (!empty($messages['check1'])) {
                        print($messages['check1']);
                        $ERROR='error';
                    }
                    if(!empty($values['check1'])){
                        $check1='checked';
                    }
                    ?>
                    <span class="<?php print $ERROR?>" >С формой ознакомлен
					    <input type="checkbox" name="check1"  value="yes" <?php print $check1?>>
                    </span>
                </div>
                </br>
                <input type="submit" value="Отправить">
			</form>
			</div>
            <?php   
                    if(!empty($_SESSION['login'])){
                        print('<form method="POST" action="login.php"><input type="submit" name="exit" value="Выход"></form>');
                    }
                ?>
		</div>		
	</body>
</html>
